package com.testproject;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.webkit.JavascriptInterface;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Ivan on 23.12.2014.
 */
public class MainActivity extends Activity {

    SharedPreferences mPref;

    Timer a_inc, b_inc;
    CountersState mCountersState = new CountersState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_app_activity);

        WebView view_a = (WebView) findViewById(R.id.webview_a);
        WebView view_b = (WebView) findViewById(R.id.webview_b);

        Button inc_a = (Button) findViewById(R.id.inc_a);
        Button inc_b = (Button) findViewById(R.id.inc_b);

        if(hasInternet()) {
            inc_a.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCountersState.deltaA(1);
                }
            });

            inc_b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCountersState.deltaB(1);
                }
            });

            initWebView(view_a, "http://jsfiddle.net/6nqrLwz9/5/show/");
            initWebView(view_b, "http://jsfiddle.net/sh55frdu/2/show");
        } else {
            Dialog dialog = new Dialog(this);
            dialog.setTitle(R.string.no_internet);
            dialog.show();
        }
    }
    private boolean hasInternet() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni != null && ni.isConnected())
            return true;

        return false;
    }

    private void initWebView(WebView webview, String url) {
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.addJavascriptInterface(mCountersState, "injectedObject");
        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        webview.loadUrl(url);
    }

    @Override
    protected void onResume() {
        mPref = this.getPreferences(Context.MODE_PRIVATE);
        mCountersState.restoreState();

        if(hasInternet())
            startTimers();
        super.onResume();
    }

    private void startTimers() {
        a_inc = new Timer();
        a_inc.schedule(new TimerTask() {
            @Override
            public void run() {
                mCountersState.deltaA(1);
            }
        }, 0, 10*1000);

        b_inc = new Timer();
        b_inc.schedule(new TimerTask() {
            @Override
            public void run() {
                mCountersState.deltaB(2);
            }
        },0,7*1000);
    }

    @Override
    protected void onPause() {
        stopTimers();

        mCountersState.saveState();

        super.onPause();
    }

    private void stopTimers() {
        if(a_inc != null)
            a_inc.cancel();
        if(b_inc != null)
            b_inc.cancel();
    }

    public class CountersState {

        AtomicInteger a = new AtomicInteger();
        AtomicInteger b = new AtomicInteger();

        @JavascriptInterface
        public void decreaseA() {
            a.decrementAndGet();
        }

        @JavascriptInterface
        public void decreaseB() {
            b.decrementAndGet();
        }

        @JavascriptInterface
        public int getActualA() {
            return a.get();
        };

        @JavascriptInterface
        public int getActualB() {
            return b.get();
        };

        public void restoreState() {
            a.set(mPref.getInt("a", 0));
            b.set(mPref.getInt("b", 0));
        }

        public void saveState() {
            mPref.edit()
                    .putInt("a", a.get())
                    .putInt("b", b.get())
                    .commit();
        }

        public void deltaA(int delta) {
            a.addAndGet(delta);
        }

        public void deltaB(int delta) {
            b.addAndGet(delta);
        }

    }

}
